#!/bin/sh
# options: -c, run the "already compiled" version, -u: run the un-compiled version and the unit-tests

if  [ $1 = '-u' ]
then
  docker load -i docker-not-compiled/hiac.tar
  ID=$(docker run -d hiac:latest)
  docker logs --follow $ID
  return 0
elif [ $1 = "-c" ]
then
    docker load -i docker-compiled/hiac.tar
    ID=$(docker run -d hiac:latest)
    docker logs --follow $ID
    return 0
fi
echo "Wrong argument"
return 1
