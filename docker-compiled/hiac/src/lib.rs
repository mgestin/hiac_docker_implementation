//! This library is an implementation of a anonymous credential, that allows to hide the Issuer of a given signature.
//!
//! # Dependencies:
//! **bls12_381 Library** is used for pairings. This library also implement modular artimethic over an Elliptic Curve.
//! The curve used is BLS12 381, a type 3 pairing friendly curve that achieves 126 bit (AES equivalent) security level.
//!  Subgroups of E(Fp^12) are all of prime order.
//!
//! **sha2 Library** is used to hash messages.
//!
//! **rand Library** for random number generation.
//!
//! (**std** dependent)

extern crate bls12_381;

use bls12_381::Scalar;


/// Number of trusted issuers for test functions..
const K_TEST: usize = 3;

/// Number of trusted issuers for benchmark functions.
const K_BENCH: usize = 100;

/// **Enum used to specify which benchmaked functions to run.**
/// Use **Long** to measure all benchmarked functions.
/// Use **Short** to skip long benchmarked functions.
pub enum BenchmarkTime{
    Long,
    Short,
}

/// Number of time to run a function to benchmark it.
const REV: u32 = 100;

pub mod ps_sig;
pub mod message;
pub mod hash;
pub mod ni_zkpoe;
pub mod ni_zkpop;
pub mod zkpok;
pub mod keygen;
pub mod signature;

/// This function produces a random bls12_381::Scalar.
fn random_scalar() -> Scalar {
    Scalar::from_raw([
        rand::random(),
        rand::random(),
        rand::random(),
        rand::random(),
    ])
}
