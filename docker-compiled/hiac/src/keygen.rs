//! Hidden Issuer Anonymous credential cryptographic keys.
extern crate bls12_381;
use crate::ni_zkpoe::NI_ZKPoE;
use crate::random_scalar;
use bls12_381::{pairing, G1Affine, G1Projective, G2Affine, G2Projective, Gt, Scalar};

/// Issuer secret key.
/// It is  built as two secret random Scalars.
#[derive(Debug)]
pub struct HIIssuerSecretKey {
    x: Scalar,
    y: Scalar,
}

/// Issuer public key.
/// It is the product of a generator of the curve, and each of the secret keys.
/// They are stored as G1Projective to reduce computation time.
/// They correspond to X^{(I)} and Y^{(I)}, for I, a given Issuer.
/// This key must be built from a secret key.
#[derive(Debug, Clone)]
pub struct HIIssuerPublicKey {
    x: G2Projective,
    y_1: G1Projective,
    y_2: G2Projective,
    x_1_bar: G1Projective,
    y_1_bar: G1Projective,
}

/// Verifier secret key AND public key
pub struct HIVerifierKeyPair {
    pub sk: HIVerifierSecretKey,
    pub pk: HIVerifierPublicKeyProd,
}

/// Hidden Issuer scheme Verifier public key.
/// Most signature schemes build the public key from the private key, in our scheme, the Verifier public key is used to build the private key.
/// This is the production key, it can be published and used in real use cases.
pub struct HIVerifierPublicKeyProd {
    // We assume that informations are ordered (w_x[i] correspond to trusted_issuers[i] etc...)
    trusted_issuers: Vec<HIIssuerPublicKey>,

    acc_x: Gt,
    acc_y: Gt,
    w_x: Vec<G1Projective>,
    w_y: Vec<G1Projective>,
    w_xp: Vec<G1Projective>,
    w_yp: Vec<G1Projective>,
    beta_x: NI_ZKPoE,
    beta_y: NI_ZKPoE,
}


/// Verifier public key, before secret key generation
/// It cannot be published, it should be used to build the verifier key.
pub struct HIVerifierPublicKeyBuild {
    // We assume that informations are ordered (w_x[i] correspond to trusted_issuers[i] etc...)
    trusted_issuers: Vec<HIIssuerPublicKey>,
    w_xp: Vec<G1Projective>,
    w_yp: Vec<G1Projective>,
}

/// Hidden Issuer scheme Verifier secret key.
/// This key must be built from a public key.
pub struct HIVerifierSecretKey {
    sk_x: Scalar,
    sk_y: Scalar,
    acc_s_x: G1Projective,
    acc_s_y: G1Projective,
}

impl HIIssuerPublicKey {
    /// Returns the parameter **x** (g_2^x) of Issuer public key.
    pub fn x(&self) -> &G2Projective {
        &self.x
    }

    /// Returns the parameter **y_1** (g_1^y) of Issuer public key.
    pub fn y_1(&self) -> &G1Projective {
        &self.y_1
    }

    /// Returns the parameter **y_2** (g_2^y) of Issuer public key.
    pub fn y_2(&self) -> &G2Projective {
        &self.y_2
    }

    /// Returns the parameter **y_1_bar** (g_1^{1/y}) of Issuer public key.
    pub fn y_1_bar(&self) -> &G1Projective {
        &self.y_1_bar
    }

    /// Returns the parameter **x_1_bar** (g_1^{1/x}) of Issuer public key.
    pub fn x_1_bar(&self) -> &G1Projective {
        &self.x_1_bar
    }
}

impl std::cmp::PartialEq for HIIssuerPublicKey {
    /// Redefinition of equality for the public key, each underlying element
    /// must be equal under the BLS12-381 equality implementation.
    fn eq(&self, other: &Self) -> bool {
        return self.x == other.x && self.y_1 == other.y_1 && self.y_2 == other.y_2;
    }
}

impl HIIssuerSecretKey {
    /// Creates a new Issuer secret key.
    /// **Non deterministic**.
    pub fn new() -> HIIssuerSecretKey {
        HIIssuerSecretKey {
            x: random_scalar(),
            y: random_scalar(),
        }
    }

    /// Generates the public key associated to the secret key &self.
    /// **Deterministic**.
    pub fn generate_public_key(&self) -> HIIssuerPublicKey {
        HIIssuerPublicKey {
            x: G2Affine::generator() * self.x,
            y_1: G1Affine::generator() * self.y,
            y_2: G2Affine::generator() * self.y,
            x_1_bar: G1Affine::generator() * self.x.invert().unwrap(),
            y_1_bar: G1Affine::generator() * self.y.invert().unwrap(),
        }
    }

    /// Returns the parameter **x** of Issuer secret key.
    pub fn x(&self) -> &Scalar {
        &self.x
    }

    /// Returns the parameter **y** of Issuer secret key.
    pub fn y(&self) -> &Scalar {
        &self.y
    }
}

impl HIVerifierKeyPair {
    /// Returns the **pk** parameter of verifier key pair.
    pub fn pk(&self) -> &HIVerifierPublicKeyProd {
        &self.pk
    }
    /// Returns the **sk** parameter of verifier key pair.
    pub fn sk(&self) -> &HIVerifierSecretKey {
        &self.sk
    }
}

impl HIVerifierPublicKeyProd {
    /// **trusted_issuers** parameter accessor.
    pub fn trusted_issuers(&self) -> &Vec<HIIssuerPublicKey> {
        &self.trusted_issuers
    }

    /// **w_x** parameter accessor.
    pub fn w_x(&self) -> &Vec<G1Projective> {
        &self.w_x
    }

    /// **w_y** parameter accessor.
    pub fn w_y(&self) -> &Vec<G1Projective> {
        &self.w_y
    }

    /// **w_xp** parameter accessor.
    /// Public verification parameter, does not include sk_x.
    pub fn w_xp(&self) -> &Vec<G1Projective> {
        &self.w_xp
    }

    /// **w_yp** parameter accessor.
    /// Public verification parameter, does not include sk_y.
    pub fn w_yp(&self) -> &Vec<G1Projective> {
        &self.w_yp
    }

    /// beta_x parameter accessor.
    fn beta_x(&self) -> &NI_ZKPoE {
        return &self.beta_x;
    }

    /// beta_y parameter accessor.
    fn beta_y(&self) -> &NI_ZKPoE {
        return &self.beta_y;
    }
    /// Verification of the integrity of the verifier public key.
    /// Must be used before the randomization of a signature with this key.
    /// **Deterministic**.
    pub fn verify_key(&self) -> bool {
        if self.beta_x().verify()
            && self.beta_y().verify()
            && *self.beta_x().y() == self.w_x
            && *self.beta_y().y() == self.w_y
        {
            return true;
        }

        return false;
    }
}

impl HIVerifierPublicKeyBuild {
    /// Trusted issuer parameter accessor.
    pub fn trusted_issuers(&self) -> &Vec<HIIssuerPublicKey> {
        &self.trusted_issuers
    }

    /// **w_xp** parameter accessor.
    /// Public verification parameter, does not include sk_x.
    pub fn w_xp(&self) -> &Vec<G1Projective> {
        &self.w_xp
    }

    /// **w_yp** parameter accessor.
    /// Public verification parameter, does not include sk_y.
    pub fn w_yp(&self) -> &Vec<G1Projective> {
        &self.w_yp
    }

    /// Build a new Verifier key.
    /// **Non deterministic**.
    pub fn new(issuers_pk: Vec<HIIssuerPublicKey>) -> HIVerifierPublicKeyBuild {
        let mut w_xp : Vec<G1Projective> = Vec::new();
        let mut w_yp : Vec<G1Projective> = Vec::new();

        for issuer in &issuers_pk{
            w_xp.push(issuer.x_1_bar.clone());
            w_yp.push(issuer.y_1_bar.clone());
        }

        let trusted_issuers = issuers_pk;
        HIVerifierPublicKeyBuild {
            trusted_issuers,
            w_xp,
            w_yp,
        }
    }

    /// Build the private key and associates a NI_ZKPoE to the public key.
    /// **Non deterministic**.
    /// A new private key built with this method would invalidate any precedently shared public key.
    pub fn generate_private_key(&mut self) -> HIVerifierKeyPair {
        let sk_x = random_scalar();
        let sk_y = random_scalar();
        let acc_s_x = G1Projective::generator() * sk_x;
        let acc_s_y = G1Projective::generator() * sk_y;

        // Copy to avoid cross references
        let beta_x = NI_ZKPoE::new(&sk_x, self.w_xp.clone());
        let beta_y = NI_ZKPoE::new(&sk_y, self.w_yp.clone());

        let mut temp_wx: Vec<G1Projective> = Vec::new();
        let mut temp_wy: Vec<G1Projective> = Vec::new();

        for (i, _) in self.w_xp.iter().enumerate() {
            temp_wx.push(self.w_xp[i] * sk_x);
            temp_wy.push(self.w_yp[i] * sk_y);
        }
        let w_x = temp_wx;
        let w_y = temp_wy;

        // Gt generator.
        let gt = pairing(&G1Affine::generator(), &G2Affine::generator());

        // pk values are cloned here, the production key and building key are independant.
        HIVerifierKeyPair {
            sk: HIVerifierSecretKey {
                sk_x,
                sk_y,
                acc_s_x,
                acc_s_y,
            },
            pk: HIVerifierPublicKeyProd {
                trusted_issuers: self.trusted_issuers.clone(),
                acc_x: gt,
                acc_y: gt,
                w_x,
                w_y,
                w_xp: self.w_xp.clone(),
                w_yp: self.w_yp.clone(),
                beta_x,
                beta_y,
            },
        }
    }
}

impl HIVerifierSecretKey {
    /// **sk_x** parameter accessor.
    pub fn sk_x(&self) -> Scalar {
        return self.sk_x;
    }

    /// **sk_y** parameter accessor.
    pub fn sk_y(&self) -> Scalar {
        return self.sk_y;
    }

    /// **acc_s_x** parameter accessor.
    pub fn acc_s_x(&self) -> G1Projective {
        return self.acc_s_x;
    }

    /// **acc_s_y** paramter accessor.
    pub fn acc_s_y(&self) -> G1Projective {
        return self.acc_s_y;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::K_TEST;

    /// Tests a Verifier key built, with various Issuers added.
    #[test]
    fn infra_build_test() {
        // Build K_TEST issuers private/ public key pairs.
        let mut issuers_pk: Vec<HIIssuerPublicKey> = Vec::new();
        for _ in 0..K_TEST {
            issuers_pk.push(HIIssuerSecretKey::new().generate_public_key());
        }
        let mut verifier_pk_build = HIVerifierPublicKeyBuild::new(issuers_pk);

        let verifier_pair = verifier_pk_build.generate_private_key();
        assert!(verifier_pair.pk().verify_key());
    }
}

pub mod _bench {
    use super::*;
    use crate::{BenchmarkTime, K_BENCH, REV};
    use std::time::Instant;

    /// Measure all operations in the keys signature implementation
    pub fn bench(bench_time: BenchmarkTime) {
        println!("______________________________________Keygen Benchmarks________________________________________________");
        _add_hoc_test();
        issuer_keygen_bench();
        verifier_keygen_one_issuer_bench();
        verifier_keygen_multi_issuer_bench();

        // Long tests
        if let BenchmarkTime::Long = bench_time {
            verifier_keygen_multi_issuer__multi_eval_bench();
            verifier_key_verification_bench();
        }
    }

    /// Measure generation of an issuer key.
    fn issuer_keygen_bench() {
        let now = Instant::now();
        for _ in 0..REV {
            let sk = HIIssuerSecretKey::new();
            let _pk = sk.generate_public_key();
        }
        let elapsed = now.elapsed();
        println!(
            "Total time (issuer keygen, {:.2?} elements) : {:.2?}. Average time (issuer keygen) {:.2?}",
            REV,
            elapsed,
            elapsed / REV
        );
    }

    /// Measures generation of a verifier key with one issuer
    fn verifier_keygen_one_issuer_bench() {
        let mut pk : Vec<HIIssuerPublicKey> = Vec::new();
        pk.push(HIIssuerSecretKey::new().generate_public_key());
        let now = Instant::now();
        for _ in 0..REV {
            HIVerifierPublicKeyBuild::new(pk.clone()).generate_private_key();
        }
        let elapsed = now.elapsed();
        println!("Total time (verifier keygen one issuer, {:.2?} elements) : {:.2?}. Average time (verifier keygen one issuer) {:.2?}", REV, elapsed, elapsed/REV);
    }

    /// Measures generation of a verifier key and multiple issuer additions.
    fn verifier_keygen_multi_issuer_bench() {
        let mut issuers_pk: Vec<HIIssuerPublicKey> = Vec::new();
        for _ in 0..K_BENCH {
            issuers_pk.push(HIIssuerSecretKey::new().generate_public_key());
        }
        let now = Instant::now();

        let mut verifier_pk = HIVerifierPublicKeyBuild::new(issuers_pk);
        verifier_pk.generate_private_key();

        let elapsed = now.elapsed();
        println!(
            "Total time (verifier keygen multi Issuer, {:.2?} Issuers) : {:.2?}",
            K_BENCH, elapsed
        );
    }

    /// Measures the keygeneration runtime for different number of trusted issuers (from 1 to 101)
    fn verifier_keygen_multi_issuer__multi_eval_bench() {
        for j in (1..(K_BENCH+2)).step_by(10){
            let mut issuers_pk: Vec<HIIssuerPublicKey> = Vec::new();
            for _ in 0..j {
                issuers_pk.push(HIIssuerSecretKey::new().generate_public_key());
            }
            let now = Instant::now();
            for _ in 0..REV{
                let mut verifier_pk = HIVerifierPublicKeyBuild::new(issuers_pk.clone());
                verifier_pk.generate_private_key();
            }

            let elapsed = now.elapsed();
            println!(
                "Total time (verifier keygen multi Issuer, {:.2?} Issuers) : {:.2?}, average time : {:.2?}.",
                j, elapsed, elapsed/REV
            );
        }
    }

    /// Measure verification of a verifier key with multiple issuers.
    /// Uses only one key Verifier key (because Verifier key production is long)
    /// But rerandomizes some elements of the key by re-generating private key each time.
    /// => We don't only mesure the verification time, but also the private key generation time.
    /// We give an estimated compensated time.
    fn verifier_key_verification_bench() {
        for j in (1..(K_BENCH+2)).step_by(10){
            let mut issuers_sk: Vec<HIIssuerSecretKey> = Vec::new();
            let mut issuers_pk: Vec<HIIssuerPublicKey> = Vec::new();
            for i in 0..j {
                issuers_sk.push(HIIssuerSecretKey::new());
                issuers_pk.push(issuers_sk[i].generate_public_key());
            }
            let mut verifier_pk = HIVerifierPublicKeyBuild::new(issuers_pk);

            let now_generate_private = Instant::now();
            for _ in 0..REV {
                verifier_pk.generate_private_key().pk();
            }

            let elapsed_generate_private = now_generate_private.elapsed();

            let mut pk_prod = verifier_pk.generate_private_key();

            let now = Instant::now();
            for _ in 0..REV {
                pk_prod.pk().verify_key();
                pk_prod = verifier_pk.generate_private_key();
            }
            let elapsed = now.elapsed();

            println!("Total time (verifier key verification, {:.2?} elements, {:.2?} loops) : {:.2?}. Average time (verifier key verification) {:.2?}. Average time, private key generation compensation : {:.2?}", j, REV, elapsed, elapsed/REV, (elapsed-elapsed_generate_private)/REV);
        }
    }

    fn _add_hoc_test()
    {
        for j in (1..(K_BENCH+350)).step_by(10){
            let now = Instant::now();
            for _ in 1..j{
                let _z = G1Projective::generator() * random_scalar();
            }
            let elapsed = now.elapsed();
            println!("Total time (hadhoc, {:.2?} elements) : {:.2?}. Average time (had oc) {:.2?}", j, elapsed, elapsed/(j as u32));
        }
    }
}
