| :exclamation:  This work is a benchmarking implementation and should not be used in production  |
|-------------------------------------------------------------------------------------------------|

Link to the original source code version (without docker images): https://gitlab.inria.fr/mgestin/rust_hidden_issuer_signature.git

The size of the repository is 2.1G. It can be long to clone !

# rust_hidden_issuer_signature

Proof of concept of the Hidden Issuer Anonymous Credential (HIAC) paper. This implementation is based on rust. It uses the BLS12-381 elliptic curve, along with a type-3 pairing. Implementation of the curve is due to zkcrypto group (https://github.com/zkcrypto/bls12_381).

## The need for an hidden issuer anonymous credential

An anonymous credential scheme requires for the Verifier to know and to trust the identity of the issuer of a credential. Indeed, the verifier need to use the issuer public key to verify a credential.

This behaviour can leak extra information about the user (place of residence, weak collusion resistance, ...).

This implementation is a POC of a new Anonymous Credential scheme that addresses these issues, by producing an anonymous credential that hides the issuer of the credential in the set of the verifier's trusted issuers.

The goal of this POC is to benchmark each algorithm of this new signature scheme, and to compare it to the well known Pointcheval Sanders Short Randomizable Signature scheme.

# How to use this Library

To build the project, simply install docker (https://docs.docker.com/get-docker/), and then run "chmod 700 hiac_docker.sh && ./hiac_docker.sh [option]". Two options :
  - -u -> the image does not have a compiled version of the code, when starting, it launches the unit tests of the program, then it compiles, and finally it runs the benchmarking program.
  - -c -> the code is already compiled, at launch, the container only runs the benchmarking program.

## License

Licensed under either of

 * Apache License, Version 2.0, (http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license (http://opensource.org/licenses/MIT)

at your option.
